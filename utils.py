# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time
import datetime

try:
    text_type = unicode
except NameError:
    text_type = str


def encode(string):
    if isinstance(string, text_type):
        return string.encode("utf-8")
    return string


def decode(string):
    if isinstance(string, text_type):
        return string
    return string.decode("utf-8")


def to_timestamp(dt):
    return time.mktime(dt.timetuple())


def parse_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(float(timestamp))


def json_serializer(value):
    if isinstance(value, datetime.datetime):
        return to_timestamp(value)
    return value
