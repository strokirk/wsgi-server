# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals

import json
from datetime import datetime
from wsgiref.simple_server import make_server

import sys
from utils import decode, encode, json_serializer, parse_timestamp

HOST = "localhost"
PORT = int("8080")
DATE_FMT = "%y%m%d"

HTTP_STATUS_CODES = {
    200: "200 OK",
    204: "204 No content",
    400: "400 Bad request",
    405: "405 Method not allowed",
}


class Storage:
    def __init__(self):
        self.values = []

    def add(self, value, timestamp):
        self.values.append([timestamp, value])

    def get_values(self, filter_date=None):
        values = sorted(self.values, key=lambda x: x[0])
        if filter_date:
            values = filter(lambda x: x[0].date() == filter_date, values)
        return list(values)


# Process-specific data storage
DB = Storage()


def app(environ, start_response):
    request = Request(environ)

    if request.method == "POST":
        response = add_event(request)
    elif request.method == "GET":
        response = get_events(request)
    else:
        # Only POST and GET are allowed
        response = Response(status=405)

    start_response(response.status, response.headers)
    return [encode(response.body)]


class Request(object):
    def __init__(self, environ):
        self.env = environ.copy()
        self.length = int(environ.get('CONTENT_LENGTH') or '0')
        self.method = environ["REQUEST_METHOD"]
        self.body = None
        if self.length:
            body = environ["wsgi.input"].read(self.length)
            self.body = decode(body)
        self.query = environ.get('QUERY_STRING', '')
        self.path = environ.get('PATH_INFO', '')

    def json(self):
        return json.loads(self.body)


class Response(object):
    def __init__(self, status, headers=(), body="", data=None):
        self._status = HTTP_STATUS_CODES[status]
        self._headers = list(headers)
        self.body = body
        if data is not None:
            # data means this is a json response
            self.body = json.dumps(data, default=json_serializer)
            self._headers += [("Content-Type", "application/json")]

    @property
    def status(self):
        if sys.version_info.major == 2:
            return encode(self._status)
        return self._status

    @property
    def headers(self):
        if sys.version_info.major == 2:
            return [(encode(k), encode(v)) for k, v in self._headers]
        return self._headers


def get_events(request):
    filter_date = None
    if request.path == "/date":
        try:
            filter_date = datetime.strptime(request.query, DATE_FMT).date()
        except ValueError:
            return Response(status=400)
    values = DB.get_values(filter_date=filter_date)
    return Response(status=200, data=values)


def add_event(request):
    data = request.json()
    try:
        value = data["value"]
        timestamp = parse_timestamp(data["timestamp"])
    except (ValueError, KeyError):
        return Response(status=400)
    DB.add(value, timestamp)
    return Response(status=204)


if __name__ == "__main__":
    print("Listening on http://%s:%d" % (HOST, PORT))

    server = make_server(HOST, PORT, app)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        raise SystemExit("Exiting")
