In this programming assignment you'll be building a simple JSON-based web application and client in
Python 2.7. The complete assignment is broken into multiple steps. Please commit each step to git
and tag it with its corresponding number. Stick with what's available in the Python standard library
documented in https://docs.python.org/2/library/, hints for which modules to use are included in the
description of the steps.

1. A basic hello server
Create a file named server.py. Use the wsgiref.simple_server module to implement a web server,
running on port 8080, that replies with "Hello World!" to any HTTP request. The Web Server Gateway
Interface (WSGI) is documented in PEP 333, and examples are also available in the library
documentation. Set it to serve requests until terminated.

2. A basic HTTP client
Create a file named client.py. Using the urllib library, write functionality to make an HTTP call to
the server and print the server reply to stdout.

3. GET and POST behaviour
Modify client.py to take an optional extra command line argument. The sys module contains a list
named argv that holds these arguments. If an extra argument is supplied, then use HTTP POST to send
it to the server, otherwise use GET and print out the reply. On the server side, modify your
implementation to detect the HTTP method, and store the string included in the POST. If the server
recieves a GET, the stored value is returned.

4. JSON replies and history
Modify your server implementation so that it returns JSON rather than plain text, see the json
module. If the server holds no data, then an empty list should be returned on GET. For every POST,
append the supplied string to the list.

5. Timestamps
Modify your server to store datestamps together with every addition. Each entry in the list should
be a list with the datestamp as the first element and the string as the second. Use the datetime
module for this.

6. Custom timestamps and sorting
Modify your client so that it takes two arguments instead of one, a timestamp and a string. Encode
those as JSON and POST them to the server. On the server side you'll need to unserialize the JSON
and append the data to your list. Keep the list sorted by timestamp. Make sure that the timestamps
are valid.

7. GET parameter
Modify your client so that if you supply only one argument, it sends a GET with the argument as a
parameter in the form "date?[argument]". Let the client verify that the argument is a valid date of
the form YYMMDD, and let the server return a sublist containing only the items with timestamps that
are within the supplied date.

8. Calendar capabilities
Change your client to conform to the following:

    Usage: python client.py add 180101-20:30 "Dinner party"
           python client.py show 180101

Also print out the above if client gets invoked in a nonconforming way. Now, instead of printing out
JSON, modify your client to behave as follows:

    [johan@bmo pythontest]$ python2 client.py add 180524-09:00 'Daily team meeting'
    Item added
    [johan@bmo pythontest]$ python2 client.py add 180524-11:30 'Lunch @ Indian King'
    Item added
    [johan@bmo pythontest]$ python2 client.py add 180525-08:00 'Make sure we follow GDPR'
    Item added
    [johan@bmo pythontest]$ python2 client.py add 180525-16:00 'Friday pub @ the office'
    Item added
    [johan@bmo pythontest]$ python2 client.py show 180524
    Schedule for 2018-05-24
    09:00    Daily team meeting
    11:30    Lunch @ Indian King
    [johan@bmo pythontest]$ python2 client.py show 180525
    Schedule for 2018-05-25
    08:00    Make sure we follow GDPR
    16:00    Friday pub @ the office

If you followed the previous steps, you shouldn't be far off in your implementation. Use
strptime/strftime to manipulate timestamps and datetime objects in the client and server. List
comprehension can also be used in a couple of places to pick apart a timestamp string.
