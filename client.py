# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import datetime
import json

from utils import decode, encode, to_timestamp, parse_timestamp

try:
    from urllib.request import urlopen
except ImportError:
    from urllib import urlopen

USAGE = """
Usage: python {0} add 180101-20:30 "Dinner party"
       python {0} show 180101
""".format(__file__)

SERVER_URL = "http://localhost:8080"


def show_events(query_date=None):
    url = SERVER_URL
    if query_date:
        url += "/date?" + query_date
    r = urlopen(url)
    body = decode(r.read())

    if 200 <= int(r.getcode()) < 300:
        events = json.loads(body)
        date = datetime.datetime.strptime(query_date, "%y%m%d").date()
        print("Schedule for %s" % date)
        for timestamp, event in events:
            dt = parse_timestamp(timestamp)
            print("%s    %s" % (dt.strftime("%H:%M"), event))
    else:
        print("Something failed, got HTTP status %s" % r.getcode())


def add_event(timestamp, value):
    try:
        added_at = datetime.datetime.strptime(timestamp, "%y%m%d-%H:%M")
    except ValueError:
        print("Error: %s is not a valid date string." % timestamp)
        return
    data = encode(json.dumps({
        "timestamp": to_timestamp(added_at),
        "value": value,
    }))
    r = urlopen(SERVER_URL, data)
    if 200 <= int(r.getcode()) < 300:
        print("Item added")
    else:
        print("Something failed, got HTTP status %s" % r.getcode())


def main(args):
    if args.command == "show":
        show_events(args.date)
    elif args.command == "add":
        add_event(args.time, args.event)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.format_usage = lambda: USAGE
    parser.format_help = lambda: USAGE
    subparsers = parser.add_subparsers()

    add_parser = subparsers.add_parser("add")
    add_parser.add_argument("time")
    add_parser.add_argument("event")
    add_parser.set_defaults(command="add")

    show_parser = subparsers.add_parser("show")
    show_parser.add_argument("date")
    show_parser.set_defaults(command="show")

    main(parser.parse_args())
