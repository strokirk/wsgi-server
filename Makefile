.PHONY: server
server:
	# Runs an automatically reloading server instance using entr
	git ls-files | entr -r python server.py

.PHONY: client
client:
	# Makes some test calls from the client
	python client.py add 180524-09:00 'Daily team meeting'
	python client.py add 180524-11:30 'Lunch @ Indian King'
	python client.py add 180525-08:00 'Make sure we follow GDPR'
	python client.py add 180525-16:00 'Friday pub @ the office 🍻'
	python client.py show 180524
	python client.py show 180525

.PHONY: client-test-errors
client-test-errors:
	-python client.py
	-python client.py add foobar xuul
	-python client.py show 20170229
